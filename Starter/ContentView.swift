//
//  ContentView.swift
//  Starter
//
//  Created by Hayk Harutyunyan on 11/29/21.
//

import SwiftUI

struct ContentView: View {
    private let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State private var percent = 0.0
    var body: some View {
        ZStack(alignment: .top) {
            Color(red: 0.071, green: 0.071, blue: 0.071).ignoresSafeArea()
            VStack {
                Image("logo")
                    .frame(width: 165, height: 165, alignment: .center)
                    .aspectRatio(contentMode: .fit)
                Rectangle().frame(width: 0, height: 24, alignment: .center)
                Image("brand")
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 300, height: 22.8, alignment: .center)
                Spacer()
                VStack {
                    
                    Text("Улучшение качества жизни")
                        .foregroundColor(Color(red: 0.922, green: 0.922, blue: 0.961, opacity: 0.6))
                        .font(.custom("Montserrat-Regular", size: 17))
                    ProgressView(value: percent, total: 100)
                        .padding([.leading, .trailing], 62)
                        .padding(.top, 11)
                        .accentColor(Color(red: 0.255, green: 0.412, blue: 0.882))
                        .scaleEffect(x: 1.0, y: 1.25, anchor: .center)
                        .onReceive(timer, perform: { _ in
                            percent += 20
                            if percent == 100 {
                                timer.upstream.connect().cancel()
                            }
                        })
                }.padding(.bottom, 64)
            }.padding(.top, 142).ignoresSafeArea()
            

        }
        

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
